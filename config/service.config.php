<?php

return array(

    'factories' => array(
        'Cms\ExtensionManager\Extension\ResponderMapper'   => 'Cms\ExtensionManager\Factory\ResponderMapperFactory',
        'Cms\ExtensionManager\Extension\Configger'         => 'Cms\ExtensionManager\Extension\ConfiggerFactory',
        'Cms\ExtensionManager\Extension\XmanagerExtension' => 'Cms\ExtensionManager\Extension\XmanagerExtensionFactory',
        'Cms\ExtensionManager\Options\ModuleOptions'       => 'Cms\ExtensionManager\Options\ModuleOptionsFactory',
        'Cms\ExtensionManager\Extension\ResponderEvent'    => 'Cms\ExtensionManager\Factory\ResponderEventFactory'
    ),

    'invokables' => array(
        'Cms\ExtensionManager\Extension\ConfiggerWriter' => 'Cms\ExtensionManager\Extension\ConfiggerWriter'
        // 'Cms\ExtensionManager\Extension\Xmanager'       => 'Cms\ExtensionManager\Extension\Xmanager', 
        //'Cms\ExtensionManager\Extension\Responder'      => 'Cms\ExtensionManager\Extension\Responder'
    ),

    'shared' => array(
        'Cms\ExtensionManager\Extension\Configger'        => false
    )
);