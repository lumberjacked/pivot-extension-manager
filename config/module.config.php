<?php

return array(
    'controller_plugins' => array(
        'factories' => array(
            'cms.extension.plugin' => 'Cms\ExtensionManager\Controller\Plugin\ExtensionPluginFactory'
        )
    ),

    'pivot' => array(
        'data' => getcwd() . '/data',
        'autoload' => getcwd() . '/config/autoload',

        'extensions' => array(
            
        )
    ),
);