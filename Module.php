<?php
namespace Cms\ExtensionManager;

use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;

class Module implements
    ConfigProviderInterface,
    ServiceProviderInterface
{

    public function onBootstrap(MvcEvent $e) {
        // $app            = $e->getApplication();
        // $serviceLocator = $app->getServiceManager();
        // $xmanager = $serviceLocator->get('Cms\ExtensionManager\Extension\XmanagerExtension');
        
        // $sharedManager = $e->getTarget()->getEventManager()->getSharedManager();
        // $sharedManager->attachAggregate($xmanager);
    }

    /**
     * Returns configuration to merge with application configuration
     *
     * @return array|\Traversable
     */

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */

    public function getServiceConfig() {
        return include __DIR__ . '/config/service.config.php';
    }
}