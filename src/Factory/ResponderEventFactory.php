<?php
namespace Cms\ExtensionManager\Factory;

use Zend\ServiceManager\ServiceLocatorInterface;
use Cms\ExtensionManager\Extension\ResponderEvent;

class ResponderEventFactory {
    
    public function __invoke(ServiceLocatorInterface $services) {
        
    	return new ResponderEvent();   
    }
}