<?php
namespace Cms\ExtensionManager\Options;

use Zend\ServiceManager\ServiceLocatorInterface;

class ModuleOptionsFactory {
    
    public function __invoke(ServiceLocatorInterface $services) {
        
        $config  = $services->get('Configuration');
        $config  = isset($config['pivot']) ? $config['pivot'] : array();
        
        return new ModuleOptions($config);
    }
}