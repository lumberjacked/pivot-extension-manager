<?php
namespace Cms\ExtensionManager\Controller\Plugin; 

use Zend\EventManager\Event;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Cms\ExtensionManager\Extension\Xmanager;

class PluginExtension extends AbstractPlugin
{
    /**
     * @var Manager
     */
    protected $xmanager;

    /**
     * @param Manager $manager
     */
    public function __construct(Xmanager $xmanager) {
        $this->xmanager = $xmanager;
    }

    public function getXmanager() {
        return $this->xmanager;
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public function __call($name, array $arguments) {
        
        return call_user_func_array(array($this->xmanager, $name), $arguments);
    }
}