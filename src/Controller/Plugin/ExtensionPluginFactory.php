<?php
namespace Cms\ExtensionManager\Controller\Plugin;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ExtensionPluginFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $pluginManager) {
        
        return new PluginExtension($pluginManager->getServiceLocator()->get('Cms\ExtensionManager\Extension\XmanagerExtension'));
    }
}