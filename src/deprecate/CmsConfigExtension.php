<?php
namespace CmsExtensionManager\Extension;

use CmsExtensionManager\Extension\AbstractExtension;
use Zend\ServiceManager\InitializerInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Config\Config;
use Zend\Config\Reader\Json as JsonReader;
use Zend\Config\Writer\Json as JsonWriter;


class CmsConfigExtension extends AbstractExtension {

	protected $installed = false;

    const LOCAL_CONFIG = 'local.php';

    protected $cmsConfig;

    protected $zfConfig;

	public function initialize(Xmanager $xmanager) {
        
        $this->zfConfig = $xmanager->getService('Config');
        $this->api      = $xmanager->get('api');
        return $this;
    }

    public function getInstalled() {
        return $this->installed;
    }

	protected function setCmsConfig(Config $config) {
        $this->cmsConfig = $config;
    }

    public function getCmsConfig() {
    	return $this->cmsConfig;
    }

    // protected function getNewCmsConfig(array $params = array()) {

    // 	$config = new Config(array(), true);
        
    //     $config->bas_cms = array();
    //     $config->bas_cms->config = $params;
        
    //     $config->doctrine = array();
    //     $config->doctrine->connection = array();
    //     $config->doctrine->connection->orm_default = array();

    //     return $config;
    // }

    // public function createCmsConfig(array $params) {

    //     $config = $this->getNewCmsConfig($params);
        
    //     $config = $this->configureDbAdapter($config);
        
    //     try{
    //         $response = $this->api->getResponse();

    //         $writer = new \Zend\Config\Writer\PhpArray();
    //         $writer->toFile($this->getOption('autoload') . '/' . static::LOCAL_CONFIG, $config->toArray());    
        	
    //     	$this->setCmsConfig($config);

    //     } catch (\Exception $e) {
    //         $response = $this->api->getErrorResponse($e->getMessage(), $config->toArray());
    //     }

    //     return $response;    
    // }

    // protected function configureDbAdapter(Config $config) {
        
    //     $dbtype = $config->bas_cms->config->get('dbtype');
        
    //     if(array_key_exists($dbtype, $this->zfConfig['doctrine']['connection'])) {
            
    //         $driver = $this->zfConfig['doctrine']['connection'][$dbtype];
            
    //         $config->doctrine->connection->orm_default = $driver;

    //     }

    //     return $config;
    // }

	// protected function createCmsConfiguration(array $params) {

 //        $response = $this->createJsonConfig($params);
        
 //        if($response->isSuccess()) {
 //            $response = $this->populateCmsConfig($params);    
 //        }

 //        return $response;
 //    }

    // protected function createJsonConfig(array $config) {

    //     $response = $this->manager->get('api')->getResponse();

    //     try{
    //         $writer = new JsonWriter();
    //         $writer->toFile($this->getOption('data') . '/' . static::CONFIG_NAME, $config);       
            
    //     } catch (\Exception $e) {
    //         $response = $this->manager->get('api')->getErrorResponse($e->getMessage());
    //     }
        
    //     return $response;
    // }

}