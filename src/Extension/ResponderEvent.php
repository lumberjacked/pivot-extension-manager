<?php
namespace Cms\ExtensionManager\Extension;

use ArrayAccess;
use Zend\Stdlib\Parameters;
use Zend\EventManager\Event;
use Zend\View\Model\JsonModel;

class ResponderEvent extends Event {
	
    public function responder($resource = null, $is_error = false, $message = null, $data = array(), $status_code = 200) {

        if($resource == null) {
            $resource = $this->getName();
        }
        
        return new Responder($resource, $is_error, $message, $data, $status_code);
    }
}