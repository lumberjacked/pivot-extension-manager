<?php
namespace Cms\ExtensionManager\Extension;

use Zend\Json\Json;
use Zend\Http\Response;
use Zend\View\Model\JsonModel;
use Zend\Stdlib\Hydrator\ClassMethods;

class Responder {

	protected $resource;

	protected $is_error;

	protected $message;

	protected $data;

	protected $status_code;

	public function __construct($resource, $is_error = false, $message = null, array $data = array(), $status_code = 200) {
		
		$this->resource    = $resource;
		$this->is_error    = $is_error;
		$this->message     = $message;
		$this->data        = $data;
		$this->status_code = $status_code;	
	}

	// public function response($error, $message = null, $data, $statusCode = 200) {

	// 	$this->setErrorStatus($error);
	// 	$this->setMessage($message);
	// 	$this->setData($data);
	// 	$this->setStatusCode($statusCode);

	// 	return $this;
	// }

	// public function setResource($resource) {
	// 	$this->resource = $resource;
	// }

	// protected function setMessage($message) {
	// 	$this->message = $message;
	// }

	// protected function setData($data) {
	// 	$this->data = $data;
	// }

	// protected function setStatuscode($statusCode) {
	// 	$this->statusCode = $statusCode;
	// }

	// protected function setErrorStatus($bool) {
	// 	$this->isError = $bool;
	// }

	public function toArray() {
		return array(
				'resource'    => $this->resource,
				'is_error'    => $this->is_error,
				'message'     => $this->message,
				'data'        => $this->data,
				'status_code' => $this->status_code
			);
	}

	public function isError() {
		return $this->is_error;
	}

	public function getResource() {
		return $this->resource;
	}

	public function getMessage() {
		return $this->message;
	}

	public function getData() {
		return $this->data;
	}

	public function getIsError() {
		return $this->is_error;
	}

	public function getStatusCode() {
		return $this->status_code;
	}

	// public function setCalledEvent($name) {
	// 	$this->name = $name;
	// }

	

	

	

	

	

	

	
}

	

	