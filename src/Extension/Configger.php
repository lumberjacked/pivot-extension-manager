<?php 
namespace Cms\ExtensionManager\Extension;

use Zend\Config\Config;
use Zend\EventManager\EventManager;

use Zend\EventManager\EventManagerInterface;
use Cms\ExtensionManager\Options\ModuleOptions;
use Zend\Config\Writer\PhpArray as ConfigWriter;
use Zend\ServiceManager\ServiceLocatorInterface;

class Configger implements ConfiggerInterface {

    protected $options;

    protected $config;

    protected $zfconfig;
    
	public function __construct(ModuleOptions $options, $zfconfig) {
        $this->options      = $options;
        $this->zfconfig     = $zfconfig;
	}

    public function getZfconfig() {
        return $this->zfconfig;
    }

    public function getConfig() {
        return $this->config;
    }

    public function setConfig(Config $config) {
        $this->config = $config;

        return $this->config;
    }

	public function getOptions() {
        return $this->options;
    }

    public function setOptions(ModuleOptions $options) {
        $this->options = $options;
    }

    public function getExtensions() {
    	return $this->options->getExtensions();
    }

    public function getAutoload() {
    	return $this->options->getAutoload();
    }

    public function getDataDirectory() {
        return $this->options->getData();
    }

    // public function getDoctrinePaths() {
    //     $zfConfig = $this->getZfConfig();
    //     return $zfConfig['doctrine']['driver']['application_entities']['paths'];
    // }

    // public function getDoctrineDriver() {
        
    //     $dbtype = $this->config['bas_cms']['config']['database']['dbtype'];
        
    //     if(array_key_exists($dbtype, $this->drivers)) {
    //          return $this->drivers[$dbtype];
    //     } 

    //     return null;

    // }

    // public function getDoctrineParams() {
    //     $dbParams = array(
    //         'driver' => $this->getDoctrineDriver()
    //     );
        
    //     $config = $this->getConfig()->toArray();
        
    //     $dbParams = array_merge($dbParams, $config['doctrine']['connection']['orm_default']['params']);
        
    //     return $dbParams;
    // }
}