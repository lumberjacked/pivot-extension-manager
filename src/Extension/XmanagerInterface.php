<?php
namespace Cms\ExtensionManager\Extension;

use Cms\ExtensionManager\Extension\ResponderEvent;
use Cms\ExtensionManager\Options\ModuleOptions;

interface XmanagerInterface {
	
	public function getConfig(ResponderEvent $e);

}