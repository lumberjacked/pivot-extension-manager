<?php
namespace Cms\ExtensionManager\Extension;

use Cms\ExtensionManager\Exception;
use Zend\EventManager\EventManager;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class XmanagerExtensionFactory implements FactoryInterface {
    
    public function createService(ServiceLocatorInterface $serviceLocator) {
        
        $eventManager = new EventManager();

        $eventManager->setEventClass($serviceLocator->get('Cms\ExtensionManager\Extension\ResponderEvent'));

        $config = $serviceLocator->get('Cms\ExtensionManager\Extension\Configger'); 
        
        $xmanager = new Xmanager($config, $eventManager);
        
        $extensions = $config->getExtensions();
        
        foreach($extensions as $name => $extension) {
            
            $xmanager->add($this->createCmsExtension($name, $extension, $serviceLocator));   
        }
        
        return $xmanager->loadExtensions();
    }

    private function createCmsExtension($name, $specs, $serviceLocator) {
        
        if(is_string($specs)) {
            $specs = array('type' => $specs);
        }
        
        $type = isset($specs['type']) ? $specs['type'] : null;
           
        if ($serviceLocator->has($type)) {
            $extension = $serviceLocator->get($type);

        } else if (class_exists($type)) {
            $extension = new $type();

        } else {
            throw new Exception\InvalidExtensionException(sprintf(
                    "An extension by the name or alias '%s' must be registered with the ServiceManager or a Fully Qualified Namespace. \n Class Not Found -- %s",
                    $name,
                    $type
                ));    
        }
        
        $traits = class_uses($extension);

        if(!$extension instanceof ExtensionInterface) {
            
            if(!array_key_exists('Cms\ExtensionManager\Extension\ExtensionTrait', $traits)) {
                
                throw new Exception\InvalidExtensionException(sprintf(
                    'An extension by the name or alias "%s" must extend AbstractExtension or implement ExtensionInterface or use ExtensionTrait in order to be valid.',
                    $name
                ));
            }
                
        }
        
        $extension->setName($name);
            
        if(array_key_exists('options', $specs)) {
            $extension->setOptions($specs['options']);
        }

        return $extension;

    }

}