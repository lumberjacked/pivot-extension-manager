<?php
namespace Cms\ExtensionManager\Extension;

use Zend\EventManager\EventManagerInterface;
use Cms\ExtensionManager\Extension\ExtensionInterface;

abstract class AbstractExtension implements ExtensionInterface {

    protected $name;

    protected $eventManager;
    
    protected $identifier;
    
    protected $options = array();

    protected $listeners = array();

    protected $hasListeners = false;

    public function __construct() {
        $this->identifer = get_called_class();
    }

    public function attach(EventManagerInterface $e) {
        
        $this->setEventManager($e);

        $listeners = $this->getListeners();

        if(is_array($listeners) && !empty($listeners)) {
            
            foreach($listeners as $event => $callback) {
                
                if(is_array($callback)) {
                    foreach($callback as $method) {
                        $this->listeners[] = $e->attach($event, array($this, $method), 100);   
                    }
                } else {
                    
                    $this->listeners[] = $e->attach($event, array($this, $callback), 100);

                }

                   
            }

            $this->hasListeners = true;
        }

        return $this;
                 
    }

    protected function getEventManager() {
        return $this->eventManager;
    }

    protected function setEventManager(EventManagerInterface $eventManager) {
        $this->eventManager = $eventManager;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getName() {
        return $this->name;  
    }

    public function setOptions(array $options) {
        
        $this->options = $options;
        return $this;
    }

    public function getOption($name) {
        $option = null;
        
        if (isset($this->options[$name])) {
            $option = $this->options[$name];
        }
        
        return $option;
    }

    public function getOptions() {
        return $this->options;
    }

    public function hasListeners() {
        return $this->hasListeners;
    }

    public function getIdentifier() {
        
        return get_called_class();
    }

    public function getListeners() {
        return $this->getOption('listeners');
    }

    protected function trigger($event, $argv = array(), $callback = array()) {
        $responseCollection = $this->getEventManager()->trigger($event, $this, $argv, $callback);
        return $responseCollection->last();
    }

    protected function get($name) {
        $responseCollection = $this->getEventManager()->trigger('get.object', $this, array('name' => $name));
        return $responseCollection->last();
    }
    
}