<?php
namespace Cms\ExtensionManager\Extension;

use Zend\Config\Config;
use Zend\Validator\File\Exists;
use Zend\EventManager\EventManager;
use Zend\Config\Writer\PhpArray as ConfigWriter;
use ZF\Configuration\ConfigResource;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ConfiggerFactory implements FactoryInterface {
    
    public function createService(ServiceLocatorInterface $serviceLocator) {
        
        $options   = $serviceLocator->get('Cms\ExtensionManager\Options\ModuleOptions');
        $configger = new Configger($options, $serviceLocator->get('Config'));
        
        return $configger;
    }
}

