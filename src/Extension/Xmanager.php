<?php

namespace Cms\ExtensionManager\Extension;

use Traversable;
use InvalidArgumentException;
use Zend\Stdlib\ArrayUtils;
use Zend\Stdlib\Parameters;
use Cms\ExtensionManager\Exception;
use Zend\EventManager\EventManagerInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class Xmanager implements  XmanagerInterface, ServiceLocatorAwareInterface {
    
    const EXTENSION_INITIALIZE = 'manager.extension.initialize';

    const EVENT_EXTENSION_LOAD = 'manager.extension.load';

    const EVENT_EXTENSION_GET  = 'manager.extension.get';

    protected $config;

    protected $services;

    protected $listeners = array(
        'get.cms.config' => 'getConfig',
        'get.object'     => 'getObject'
        );

    protected $extensions = array();

    protected $identifiers = array();

    protected $eventManager;

    
    public function __construct(Configger $config, EventManagerInterface $eventManager) {
        $this->config        = $config;
        $this->attach($eventManager);
        $this->identifiers[] = get_called_class();
        
    }

    public function getConfig(ResponderEvent $e) {
         return $this->setConfig($this->services->get('Cms\ExtensionManager\Extension\Configger'));
    }

    public function setConfig($config) {
        $this->config = $config;
        return $this->config;
    }

    protected function attach(EventManagerInterface $e) {
        
        $this->setEventManager($e);

        $listeners = $this->getListeners();
        if(is_array($listeners) && !empty($listeners)) {
            
            foreach($listeners as $event => $callback) {
                
                $this->listeners[] = $e->attach($event, array($this, $callback), 100);   
            }
        }

        return $this;
                 
    }

    public function getEventManager() {
        return $this->eventManager;    
    }

    protected function setEventManager(EventManagerInterface $eventManager) {
        $this->eventManager = $eventManager;
    }

    protected function getListeners() {
        return $this->listeners;
    }

    public function add($extension) {
        
        $extension->attach($this->getEventManager());
        $this->extensions[$extension->getName()] = $extension;

        return $this;
    }

    public function loadExtensions() {
        
        $this->getEventManager()->trigger(static::EVENT_EXTENSION_LOAD);
        return $this;
    }

    public function getExtensions() {
        return $this->extensions;
    }

    

    public function getServiceLocator() {
        return $this->services;
    }

    public function setServiceLocator(ServiceLocatorInterface $services) {
        $this->services = $services;
    }

    protected function setIdentifier($name) {
        $this->identifiers[] = $name;
    }

    protected function getIdentifiers() {
        return $this->identifiers;
    }

    public function has($name) {
        return (array_key_exists($name, $this->getExtensions()) ? true : false);
    }

    public function get($name) {
        
        if($this->has($name)) {
            
            $extension = $this->extensions[$name];
        
            $this->getEventManager()->trigger(static::EVENT_EXTENSION_GET, $extension);

            return $extension;

        } else if($this->getServiceLocator()->has($name)) {
            
            return $this->getServiceLocator()->get($name);
        
        } else {

            throw new Exception\MissingExtensionException(sprintf(
                    'An extension or service by the name/alias "%s" does not exist',
                    $name
                ));

        }  
    }

    public function getObject(ResponderEvent $e) {
        $name = $e->getParam('name');
        
        if($this->has($name)) {
            
            $extension = $this->extensions[$name];
        
            $this->getEventManager()->trigger(static::EVENT_EXTENSION_GET, $extension);

            return $extension;

        } else if($this->getServiceLocator()->has($name)) {
            
            return $this->getServiceLocator()->get($name);
        
        } else {
            
            throw new Exception\MissingExtensionException(sprintf(
                    'An Extension, Service, or Entity by the name/alias "%s" does not exist',
                    $name
                ));

        }  
    }

    public function getExtension($name) {
        if($this->has($name)) {
            
            $extension = $this->extensions[$name];
        
            $this->getEventManager()->trigger(static::EVENT_EXTENSION_GET, $extension);

            return $extension;

        } else {

            throw new Exception\MissingExtensionException(sprintf(
                    'An extension or service by the name/alias "%s" does not exist',
                    $name
                ));

        }     
    }

    public function trigger($event, DtoExtensionInterface $dto, $callback = array()) {
        
        $responder = $this->getEventManager()->trigger($event, $this, $dto, $callback);
        return $responder->last();
    }

    public function responder($event, $params) {
        return $this->trigger($event, $params);
    }
    
    public function api($method, $resource, array $params = array()) {

        $config = $this->trigger('get.cms.config');
        
        $requester = new Requester($config->getApiBackend(), $resource, $method, $params);
        $responder = $this->getEventManager()->trigger('api', $this, $requester);
        
        return $responder->last();    
    }

    public function db($method = null, $resource = null, array $params = array()) {
        
        $requester = new DbRequester($method, $resource, $params);
        $responder = $this->getEventManager()->trigger('db.event', $this, $requester);
        
        return $responder->last();
        
        
    }

    public function client($event, $method = null, $resource = null, array $params = array()) {
        $config      = $this->trigger('get.cms.config');
        $api_backend = $config->getApiBackend();
        
        if(null == $api_backend) {
            if(array_key_exists('fqdn_api', $params)) {
                $api_backend = $params['fqdn_api'];
            }
            
        }
        
        $requester = new Requester($api_backend, $resource, $method, $params);
        $responder = $this->getEventManager()->trigger($event, $this, $requester);

        return $responder->last();   
    }

    

    
}