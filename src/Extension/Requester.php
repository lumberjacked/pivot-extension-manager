<?php
namespace Cms\ExtensionManager\Extension;

use Zend\Json\Json;
use Zend\Http\Client;
use Zend\Http\Response;

class Requester {

	protected $fqdn;

	protected $method;
	
	protected $resource;

	protected $fqdn_resource;

	protected $params;

	protected $client;

	protected $headers = array();
	

	public function __construct($fqdn, $resource, $method, array $params = array()) {

		$this->fqdn     = $fqdn;
		$this->resource = $resource;
		$this->method   = $method;
		
		$this->parseParams($params);
		$this->initRequester();

	}

	public function newRequester($resource, $method, array $params = array()) {
		$this->resource = $resource;
		$this->method   = $method;
		
		$this->headers = array();
		$this->parseParams($params);
		$this->initRequester();

		return $this;	
	}

	public function request() {
		$client = $this->getClient();
		
		return $this->hydrate($client->send());
	}

	protected function hydrate(Response $response) {
		
		// If request failed flag it and send back error response
		if(!$response->isSuccess()) {
			$response = Json::decode($response->getContent(), Json::TYPE_ARRAY);
			return new Responder($this->getFqdnResource(), true, $response['title'], array(), $response['status']);
		}

		$response = Json::decode($response->getContent(), Json::TYPE_ARRAY);

		// If access_token is available return special request for access
		if(array_key_exists('access_token', $response)) {	
			return new Responder($this->getFqdnResource(), false, 'Received valid access token', array('access_token' => $response['access_token']));
		}
		

		return new Responder($response['resource'], $response['is_error'], $response['message'], $response['data'], $response['status_code']);

	}

	protected function getClient() {
		return $this->client;
	}

	protected function setFqdnResource($fqdn) {
		$this->fqdn_resource = $fqdn;
	}

	public function getFqdnResource() {
		return $this->fqdn_resource;
	}

	protected function getMethod() {
		return $this->method;
	}

	protected function setClient($client) {
		$this->client = $client;
	}

	protected function parseParams(array $params) {

		if(array_key_exists('headers', $params)) {
			$headers = $params['headers'];
			$this->setHeaders($headers);
			unset($params['headers']);
		}

		$this->setParams($params);

	}

	public function checkAvailableParams(array $keys) {
		$params = $this->getParams();
		foreach($keys as $index => $key) {
			if(!array_key_exists($key, $params)) {
				return new \Exception(sprintf('This client api call requires %s -- none given', $key));
			}
		}

		return true;
	}

	protected function setParams($params) {
		$this->params = $params;
	}

	public function getParams() {
		return $this->params;
	}

	public function getParam($name) {
		if(array_key_exists($name, $this->getParams())) {
			return $this->params[$name];
		}

		return null;
	}

	protected function setHeaders($headers) {
		$this->headers = $headers;
	}

	protected function getHeaders() {
		return $this->headers;
	}


	protected function initRequester() {
        
        if($this->fqdn == null) {
       		throw new \Exception('FQDN must be defined!  Most lickly you need to install the cms or re-generate your configuration');
        }
            
        $this->setFqdnResource($this->fqdn . '/' . $this->resource);
        
        $client = $this->buildApiClient();

        $this->setClient($client);

        return $this;
    }

	protected function buildApiClient() {
        
        $client = new Client($this->getFqdnResource());
        $client->setAdapter('Zend\Http\Client\Adapter\Curl');

        if(in_array($this->getMethod(), array('post', 'put', 'delete'))) {
            $client->setMethod($this->getMethod());
            $client->setParameterPost($this->getParams());    
        }

        $client_headers = $client->getRequest()->getHeaders();
        $client_headers->addHeaderLine('Accept', 'application/json');
        
        if(!empty($this->getHeaders())) {
        	$headers = $this->getHeaders();
        	
        	if(array_key_exists('auth', $headers)) {
        		$auth = $headers['auth'];
        		$client->setAuth($auth['email'], $auth['password']);
        		unset($headers['auth']);
        	}

        	if(!empty($headers)) {
        		
        		foreach($headers as $key => $value) {
        			if($value !== 'application/json') {
        				$client_headers->addHeaderLine($key, $value);	
        			}
        		}
        	}

            
        }

        return $client;
    }


	
}

	

	