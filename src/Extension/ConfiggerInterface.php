<?php
namespace Cms\ExtensionManager\Extension;

use Cms\ExtensionManager\Options\ModuleOptions;

interface ConfiggerInterface {
	
	public function getOptions();

    public function setOptions(ModuleOptions $options);

}