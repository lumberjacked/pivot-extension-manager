<?php
namespace Cms\ExtensionManager\Extension;

interface ExtensionInterface  {
    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     * @throws Exception\InvalidOptionException on invalid option name
     * @return mixed
     */
    public function getOption($name);

    /**
     * @param string $name
     * @return mixed
     */
    public function getOptions();

    /**
     * @param array $options
     * @return $this
     */
    public function setOptions(array $options);

}