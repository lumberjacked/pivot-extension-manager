<?php

namespace Cms\ExtensionManager\Exception;

class InvalidExtensionException extends \RuntimeException
{
}